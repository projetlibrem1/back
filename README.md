# Projet AcLab Master 1 MIII
Ce repository est le *back* de l'application.
Le groupe se constitue de :
* Chiara Brichot (@Mid0na / GitLab)
* Thais Révillon (@Thaisr / GitLab)
* Adrien Lecomte (@lecomtea / GitLab)
* Jean-François Gautreau (@jeff59550 / GitLab)
* Ahlem Fahem (@hallouma.fahem1 / GitLab)
* Lamya Rayess (@lamya-rey / GitLab)

## Install
Run `npm install`

## Database
Configuration of the Database on `config/config.json`

## Run
Run server with `npm rus start`
