const mariadb = require('mariadb/callback');
module.exports = {
    HOST: "localhost",
    USER: "root",
    DB: "storiesDB",
    PASSWORD: "",
    dialect: "mariadb",
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000
    }
  };
