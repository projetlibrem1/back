const { authJwt } = require("../middleware");
const controller = require("../controllers/chapter.controller");

module.exports = function(app) {

//get all chapter
//access for moderator and admin only
app.get("/chapters",[authJwt.verifyToken, authJwt.isModeratorOrAdmin],
controller.getAllChapters
);

//with token
app.get("/chapters/:chapter_id",
[authJwt.verifyToken],
controller.getChapterById
);

app.get("/chapters/active/:story_id",
    controller.getChapterByStoryIdAndStatusActive
);

app.get("/chapters/order/:story_id/:order", controller.getChapterByOrder)

//without token
app.get("/chapters/status/:status",
controller.getAllChaptersByStatus
);

//with token
app.get("/chapters/story/:story_id",
[authJwt.verifyToken],
controller.getChapterByStoryId
);

//with token
app.put("/chapter/:chapter_id",
  [authJwt.verifyToken],
  controller.updateChapter
  );

  //with token
  app.post("/create/chapter",
  [authJwt.verifyToken],
  controller.createChapter
  );

  //with token
  app.delete("/chapter/delete/:chapter_id",
      [authJwt.verifyToken],
      controller.deleteChapterById
  );

  //with token
  app.get("/chapter/order",
      [authJwt.verifyToken],
      controller.getAllChaptersByOrder
  );
}
