const { authJwt } = require("../middleware");
const controller = require("../controllers/part.controller");

module.exports = function(app) {
    app.use(function (req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });


    app.get("/parts",
        controller.getAllParts
    );

    app.get("/part/:partId",
        [authJwt.verifyToken],
        controller.getPartById
    );

    app.post("/part",
        [authJwt.verifyToken],
        controller.createPart
    );

    app.put("/part/:id",
        [authJwt.verifyToken],
        controller.updatePart
    );


    app.delete("/part/delete/:partId",
        [authJwt.verifyToken],
        controller.deletePartById
    );
}