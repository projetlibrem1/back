const { authJwt } = require("../middleware");
const controller = require("../controllers/choice.controller");

module.exports = function(app) {
    app.use(function(req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });


app.get("/choices",
    controller.getAllChoices
);

app.get("/choice/:choiceId",
    [authJwt.verifyToken],
    controller.getChoiceById);

app.post("/choice",
    [authJwt.verifyToken],
    controller.createChoice
);

app.put("/choice/:id",
    [authJwt.verifyToken],
    controller.updateChoice
);

app.delete("/choice/delete/:choiceId",
    [authJwt.verifyToken],
    controller.deleteChoiceById
);
}