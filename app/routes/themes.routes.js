const { authJwt } = require("../middleware");
const controller = require("../controllers/themes.controller");
const upload = require("../middleware/upload");

module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  app.post("/create/theme",
  [authJwt.verifyToken], upload.single("file"),controller.createTheme);
  
  app.get("/themes",
    controller.allThemes
  );

  app.get("/theme/:themeId",
  [authJwt.verifyToken], 
  controller.getThemeById);
};
