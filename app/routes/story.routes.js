const { authJwt } = require("../middleware");
const controller = require("../controllers/stories.controller");

module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });


  app.get("/stories/recent/posted",
    [authJwt.verifyToken],
    controller.get3LastPostedStories
  );
  app.get("/theme/stories/:theme_id",
      [authJwt.verifyToken],
      controller.getStoriesByTheme
  );
  app.get("/stories/active", controller.allActiveStories);
  app.get("/stories/author/:authorId",
  [authJwt.verifyToken],
  controller.getStoriesByAuthorid
  );
  app.get("/story/updated/:authorId",
  [authJwt.verifyToken],
  controller.getLastUpdatedStory
  );
  app.get("/story/:storyId",
      [authJwt.verifyToken],
      controller.getStoryById);

  app.get("/stories/:category",
      [authJwt.verifyToken],
      controller.getStoriesByCategory
  );
  app.post("/story",
  [authJwt.verifyToken],
  controller.createStory
  );
  app.get("/stories",
      [authJwt.verifyToken],
      controller.allStories
  );


  app.put("/story/:id",
  [authJwt.verifyToken],
  controller.updateStory
  );

  app.get("/stories/:status",
   controller.getStoriesByStatus
  );
};
