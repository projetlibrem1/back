const { authJwt } = require("../middleware");
const controller = require("../controllers/user.controller");

module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  app.get("/api/test/all", controller.allAccess);

  //get users list
  app.get(
    "/api/test/user",
    [authJwt.verifyToken],
    controller.userBoard
  );

   //get moderator list
  app.get(
    "/api/test/mod",
    [authJwt.verifyToken, authJwt.isModerator],
    controller.moderatorBoard
  );

  //get admin list
  app.get(
    "/api/test/admin",
    [authJwt.verifyToken, authJwt.isAdmin],
    controller.adminBoard
  );
  app.get("/api/user/me",
  [authJwt.verifyToken],
   controller.userContent);
  
   app.get("/api/users",
   [authJwt.verifyToken, authJwt.isAdmin],
    controller.allUsers);
};