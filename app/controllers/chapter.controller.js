const { chapter } = require("../models");
const db = require("../models");
const Chapter = db.chapter;

exports.getAllChapters = (req,res) => {
    Chapter.findAll().then( chapters => {
        res.status(200).json({
            chapters
          });
        
    }).catch(err => {
      res.status(500).json({
        "description": "Can not access all chapters",
        "error": err
      });
    })
  }
exports.getAllChaptersByOrder = (req,res) => {
    Chapter.findAll({ order: [
            ['order', 'ASC']]}).then( chapters => {
        res.status(200).json({
            chapters
        });

    }).catch(err => {
        res.status(500).json({
            "description": "Can not access all chapters",
            "error": err
        });
    })
}
exports.getAllChaptersByStatus = (req,res) => {
    Chapter.findAll({
        where: {status: req.params.status}
      }).then( chapters => {
        res.status(200).json({
            chapters
          });
        
    }).catch(err => {
      res.status(500).json({
        "description": "Can not access all chapters",
        "error": err
      });
    })
  }
exports.getChapterById = (req, res) => {
    Chapter.findOne({
      where: {id: req.params.chapter_id}
    }).then(chapter => {
        res.status(200).send({
            chapter
          });
    }).catch(err => {
      res.status(500).json({
        "description": "Can not access Chapter Page",
        "error": err
      });
    })
}
exports.getChapterByStoryId = (req, res) => {
    Chapter.findAll({
      where: {storyId: req.params.story_id}
    }).then(chapter => {
        res.status(200).send({
            chapter
          });
    }).catch(err => {
      res.status(500).json({
        "description": "Can not access Chapter Page",
        "error": err
      });
    })
}

exports.getChapterByStoryIdAndStatusActive = (req, res) => {
    Chapter.findAll({
      where: {storyId: req.params.story_id, status: 'active'},
        order: [ [ 'order', 'DESC' ]]

    }).then(chapter => {
        res.status(200).send({
            chapter
          });
    }).catch(err => {
      res.status(500).json({
        "description": "Can not access Chapter Page",
        "error": err
      });
    })
}

exports.getChapterByOrder = (req, res) => {
    console.log('yo')
    Chapter.findOne({
        where: {storyId: req.params.story_id, order: req.params.order},
        order: [ [ 'order', 'DESC' ]]

    }).then(chapter => {
        res.status(200).send({
            chapter
        });
    }).catch(err => {
        res.status(500).json({
            "description": "Can not access Chapter Page",
            "error": err
        });
    })
}

exports.updateChapter = (req, res) => {
    const id = req.params.chapter_id;
    Chapter.update(req.body, {
      where: { id: id },
      chapter_name: req.body.chapter_name,
      text: req.body.text,
      order: req.body.order,
      status: req.body.status,
      story_id: req.body.story_id
    }).then(chapter => {
      Chapter.findOne({
        where: {id: id}
      }).then(chapter => {
        res.status(200).send({
            id: chapter.id,
            chapter_name: chapter.chapter_name,
            text: chapter.text,
            order: chapter.order,
            status: chapter.status,
            storyId : chapter.storyId,
            updatedAt:chapter.updatedAt,
            createdAt:chapter.createdAt
          });     
      })
    }).catch(err => {
        res.status(500).send({
          message: "Error updating chapter with id=" + id + err
        });
      });
  };
exports.createChapter = (req, res) => {
    // Save chapter to Database
    console.log('in creaete chapter')
    Chapter.findAll({
        where : {story_id: req.body.story_id},
        limit: 1,
        order: [ [ 'createdAt', 'DESC' ]]
    }).then(function(entries){
        console.log('creating')
      Chapter.create({
        chapter_name: req.body.chapter_name,
        text: req.body.text,
        status: req.body.status,
        order : entries[0].order+1,
        storyId : req.body.story_id
    })
    .then(chapter => {
        console.log('hey')
        res.status(200).send({
            id: chapter.id,
            chapter_name: chapter.chapter_name,
            text: chapter.text,
            order: chapter.order,
            status: chapter.status,
            storyId : chapter.storyId,
            updatedAt:chapter.updatedAt,
            createdAt:chapter.createdAt
          });
    })
    .catch(err => {
        console.log('something is wrong')
        res.status(500).send({ message: err.message });
    });
    }).catch(err => {
      Chapter.create({
        chapter_name: req.body.chapter_name,
        text: req.body.text,
        status: req.body.status,
        order : 1,
        storyId : req.body.story_id
    })
    .then(chapter => {
        res.status(200).send({
            id: chapter.id,
            chapter_name: chapter.chapter_name,
            text: chapter.text,
            order: chapter.order,
            status: chapter.status,
            storyId : chapter.storyId,
            updatedAt:chapter.updatedAt,
            createdAt:chapter.createdAt
          });
    })
    .catch(err => {
        res.status(500).send({ message: err.message });
    });
    });
   
  };

exports.deleteChapterById = (req,res) => {
    Chapter.destroy({
      where: {id: req.params.chapter_id}
    }).then(_ => {
        res.status(200).json({
          "description": "chapter is deleted !"
        });
    }).catch(err => {
      res.status(500).json({
        "description": "Can not delete  chapter by id",
        "error": err
      });
    })
  }
