const { choice } = require("../models");
const db = require("../models");
const Choice = db.choice;

exports.createChoice = (req, res) => {
    Choice.create({
        label: req.body.label
    })
        .then(choice => {
                    res.status(200).send({
                        id: choice.id,
                        label: choice.label
                    });
                })
        .catch(err => {
            res.status(500).send({ message: err.message });
        });
};

exports.getAllChoices = (req,res) => {
    Choice.findAll().then( choices => {
        res.status(200).json({
            choices
        });

    }).catch(err => {
        res.status(500).json({
            "description": "Can not access all choices",
            "error": err
        });
    })
}

exports.getChoiceById = (req, res) => {
    Choice.findOne({
        where: {id: req.params.choice_id}
    }).then(choice => {
        res.status(200).send({
            choice
        });
    }).catch(err => {
        res.status(500).json({
            "description": "Can not access Choice Page",
            "error": err
        });
    })
}

exports.updateChoice = (req, res) => {
    const id = req.params.choice_id;
    Choice.update(req.body, {
        where: { id: id },
        label: req.body.label
    }).then(choice => {
        Choice.findOne({
            where: {id: id}
        }).then(choice => {
            res.status(200).send({
                id: choice.id,
                label: choice.label
            });
        })
    }).catch(err => {
        res.status(500).send({
            message: "Error updating choice with id=" + id + err
        });
    });
};

exports.deleteChoiceById = (req,res) => {
    Choice.destroy({
        where: {id: req.params.choice_id}
    }).then(_ => {
        res.status(200).json({
            "description": "choice is deleted !"
        });
    }).catch(err => {
        res.status(500).json({
            "description": "Can not delete  choice by id",
            "error": err
        });
    })
}