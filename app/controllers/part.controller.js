const { part } = require("../models");
const db = require("../models");
const Part = db.part;

exports.createPart = (req, res) => {
    // Save part to Database
    Part.create({
        text: req.body.text,
        authorDescritpion: part.authorDescritpion,
        chapterId : req.body.chapterId

    })
        .then(part => {
            res.status(200).send({
                id: part.id,
                text: part.text,
                authorDescritpion: part.authorDescritpion,
                chapterId : req.body.chapterId
            });
        })
        .catch(err => {
            res.status(500).send({ message: err.message });
        });
};

exports.getAllParts = (req,res) => {
    Part.findAll().then( parts => {
        res.status(200).json({
            parts
        });

    }).catch(err => {
        res.status(500).json({
            "description": "Can not access all parts",
            "error": err
        });
    })
}

exports.getPartById = (req, res) => {
    Part.findOne({
        where: {id: req.params.part_id}
    }).then(part => {
        res.status(200).send({
            part
        });
    }).catch(err => {
        res.status(500).json({
            "description": "Can not access part Page",
            "error": err
        });
    })
}

exports.updatePart = (req, res) => {
    const id = req.params.part_id;
    Part.update(req.body, {
        where: { id: id },
        text: req.body.text,
        authorDescritpion: part.authorDescritpion,
        chapterId : req.body.chapterId,
        updatedAt:part.updatedAt,
        createdAt:part.createdAt
    }).then(Part => {
        Part.findOne({
            where: {id: id}
        }).then(part => {
            res.status(200).send({
                id: part.id,
                text: part.text,
                authorDescritpion: part.authorDescritpion,
                chapterId : req.body.chapterId,
                updatedAt:part.updatedAt,
                createdAt:part.createdAt
            });
        })
    }).catch(err => {
        res.status(500).send({
            message: "Error updating part with id=" + id + err
        });
    });
};

exports.deletePartById = (req,res) => {
    Part.destroy({
        where: {id: req.params.part_id}
    }).then(_ => {
        res.status(200).json({
            "description": "part is deleted !"
        });
    }).catch(err => {
        res.status(500).json({
            "description": "Can not delete  part by id",
            "error": err
        });
    })
}