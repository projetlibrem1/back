const { DataTypes } = require("sequelize");

module.exports = (sequelize, Sequelize) => {
    const Theme = sequelize.define("theme", {
      name: {
        type: Sequelize.STRING
      },
      img: {
        type: DataTypes.BLOB("long"), allowNull: true,

      }
    });
  
    return Theme;
  };
