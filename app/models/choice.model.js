const { DataTypes } = require("sequelize");

module.exports = (sequelize, Sequelize) => {
    const Choice = sequelize.define("choices", {
        label: {
            type: Sequelize.STRING
        }
    });

    return Choice;
};
