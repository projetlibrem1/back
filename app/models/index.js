  
const { Sequelize } = require("sequelize");
const config = require("../config/db.config.js");

const sequelize = new Sequelize(
  config.DB,
  config.USER,
  config.PASSWORD,
  {
    host: config.HOST,
    dialect: config.dialect,
    operatorsAliases: false,
    pool: {
      max: config.pool.max,
      min: config.pool.min,
      acquire: config.pool.acquire,
      idle: config.pool.idle
    }
  }
);

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.user = require("../models/user.model.js")(sequelize, Sequelize);
db.role = require("../models/role.model.js")(sequelize, Sequelize);
db.story = require("../models/story.model.js")(sequelize, Sequelize);
db.theme = require("../models/theme.model.js")(sequelize, Sequelize);
db.chapter = require("../models/chapter.model.js")(sequelize, Sequelize);
db.choice = require("../models/choice.model.js")(sequelize, Sequelize);
db.part = require("../models/part.model.js")(sequelize, Sequelize);

db.user.hasMany(db.story, { as: "readedStories" });
db.user.hasMany(db.story, { as: "updatedStories" });

db.role.belongsToMany(db.user, {
  through: "user_roles",
  foreignKey: "roleId",
  otherKey: "userId"
});
db.user.belongsToMany(db.role, {
  through: "user_roles",
  foreignKey: "userId",
  otherKey: "roleId",
});

db.part.belongsToMany(db.part,{
    through: "part_part",
    foreignKey: "parent_part_id",
    otherKey: "child_part_id",
    as : "parent",


});

db.part.belongsToMany(db.part,{
    through: "part_part",
    foreignKey: "child_part_id",
    otherKey: "parent_part_id",
    as : "child",
});


db.part.hasMany(db.choice, { as: "choices" });

db.story.hasMany(db.chapter, { as: "chapters" });
db.story.belongsTo(db.user, {foreignKey: 'userId', as: 'User'})
db.story.belongsTo(db.theme, {foreignKey: 'theme_id', as: 'Theme'})


db.chapter.hasMany(db.part, { as: "parts" });


db.ROLES = ["user", "admin", "moderator"];

db.reading = ["user", "story", "moderator"];

module.exports = db;
