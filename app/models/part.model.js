const { DataTypes } = require("sequelize");

module.exports = (sequelize, Sequelize) => {
    const Part = sequelize.define("parts", {
        text: {
            type: DataTypes.TEXT
        },
        authorDescritpion: {
            type: DataTypes.TEXT
        }
    });

    return Part;
};