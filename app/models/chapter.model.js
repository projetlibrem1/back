const { DataTypes } = require("sequelize");

module.exports = (sequelize, Sequelize) => {
    const Chapter = sequelize.define("chapters", {
      chapter_name: {
        type: Sequelize.STRING
      },
      text: {
        type: DataTypes.TEXT
      },
      order: {
        type: DataTypes.INTEGER
      },
      status: {
        type: Sequelize.STRING
      }
    });

    return Chapter;
  };
