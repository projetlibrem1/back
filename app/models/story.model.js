const { DataTypes } = require("sequelize");

module.exports = (sequelize, Sequelize) => {
    const Story = sequelize.define("stories", {
      title: {
        type: Sequelize.STRING
      },
      category: {
        type: Sequelize.STRING
      },
      average_rate: {
        type: DataTypes.FLOAT
      },
      average_rate_count: {
        type: DataTypes.INTEGER
      },
      tags: {
        type: Sequelize.STRING
      },
      status: {
        type: Sequelize.STRING
      },
      seenCount :  {
        type: DataTypes.BIGINT
      }
    });

    return Story;
  };
